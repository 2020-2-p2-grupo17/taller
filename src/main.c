#include <stdio.h>
#include <stdlib.h>
#include "bytes.h"

int main()  {

    int *arr = (int *)calloc(10,sizeof(int));
    if (arr == NULL) { 
        printf("Memory not allocated.\n"); 
    } 
    
    for (int i =0; i<10;i++){
    	int entero;
    	printf("Introduce un entero:\n");
  	scanf("%d", &entero);
    	*(arr+i)= entero;
    }
    free(arr);
    
    typedef struct datos{
    int a;
    char *b;
    }tba;

    tba n;
    n.a = 1245;
    n.b = "hola";

    printf("Estructura\n");
    printf("Tipo dato: int \n");
    mostrarBytes(&n.a, sizeof(n.a));
    printf("\nTipo dato: str\n");
    mostrarBytes(&n.b, sizeof(n.b));
    return 0;

}

