bin/taller: obj/main.o obj/bytes.o
	gcc -Wall -fsanitize=address,undefined $^ -o bin/taller

obj/main.o: src/main.c
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

obj/bytes.o: src/bytes.c
	gcc -Wall -c -I include/ src/bytes.c -o obj/bytes.o

clean:
	rm -rf obj bin
	mkdir obj bin
